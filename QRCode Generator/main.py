import qrcode
import sys
import os

MAX_TEXT_SIZE = 60

if len(sys.argv) < 2:
    print("pass the text as argument.")
    exit(1)
txt =  sys.argv[1]
qr = qrcode.QRCode(box_size=MAX_TEXT_SIZE,)
qr.add_data(txt)
qr.make(fit=True)
img = qr.make_image(fill_color="black", back_color="white")
img.save('img/img.jpg', 'JPEG')
os.system('chmod a+=rwx img/img.jpg')
