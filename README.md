# Develop cpu docker and run on gpu
This codebase show how to run a container with tensorflow on Docker and Nvidia-Docker withouth duplicate files.

## With Shovel
```
cd DockerCPU-GPU(shovel_version)
pip install -r dev-requirements.txt
shovel docker.run_cpu
# to kwnow all tasks
shovel tasks
```

## With makefile
```
cd DockerCPU-GPU(make_file_version)
make dockerfile_cpu
make dockerfile_gpu
docker-compose build
docker-compose run cpu
docker-compoe run gpu
```

## Run Slide
```
cd Slide
docker-compose up
firefox localhost:8000
```

## Generate QR code
```
cd QRCode Generator
docker-compose run qr "text"
# img.jpg
```
