import os
from shovel import task

GPU_DOCKERFILE_SOURCE = 'dockerfile_source/gpu.docker'
GPU_DOCKERFILE_TARGET = 'dockerfile_compiled/GPU-Dockerfile'
CPU_DOCKERFILE_SOURCE = 'dockerfile_source/cpu.docker'
CPU_DOCKERFILE_TARGET = 'dockerfile_compiled/CPU-Dockerfile'

def build_dockerfile(source, target):
    os.system('pcpp -o %s %s' % (target, source))

def docker_compose_build(variant):
    os.system('docker-compose build %s' % variant)

def docker_compose_run(variant):
    os.system('docker-compose run %s' % variant)

@task
def build_cpu():
    """
    This build cpu dockerfiled. Esamples: shovel docker.build_cpu
    """
    build_dockerfile(CPU_DOCKERFILE_SOURCE, CPU_DOCKERFILE_TARGET)

@task
def build_gpu():
    """
    This build gpu dockerfiled. Esamples: shovel docker.build_gpu
    """
    build_dockerfile(GPU_DOCKERFILE_SOURCE, GPU_DOCKERFILE_TARGET)

@task
def run_cpu():
    """
    This run cpu container. Esamples: shovel docker.run_cpu
    """
    build_cpu()
    docker_compose_build('cpu')
    docker_compose_run('cpu')

@task
def run_gpu():
    """
    This run gpu container. Esamples: shovel docker.run_gpu
    """
    build_gpu()
    docker_compose_build('gpu')
    docker_compose_run('gpu')

@task
def exec_cpu():
    """
    This run a bash on cpu container. Esamples: shovel docker.exec_cpu
    """
    build_cpu()
    docker_compose_build('cpu')
    docker_compose_run('cpu bash')

@task
def exec_gpu():
    """
    This run a bash on gpu container. Esamples: shovel docker.exec_gpu
    """
    build_gpu()
    docker_compose_build('gpu')
    docker_compose_run('gpu bash')